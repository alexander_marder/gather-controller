#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update
sudo apt-get install -y ca-certificates curl gnupg python3 python3-pip openssh-server htop nano

## Add Docker's official GPG key:
#sudo install -m 0755 -d /etc/apt/keyrings
#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
#sudo chmod a+r /etc/apt/keyrings/docker.gpg
#
## Add the repository to Apt sources:
#echo \
#  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
#  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
#  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
#sudo apt-get update
#sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
#sudo usermod -aG docker $USER
#newgrp docker

cd ~

git clone https://gitlab.com/alexander_marder/usrp-record.git
cd usrp-record
sudo docker build -t record .
sudo ln -s record.sh /usr/local/bin/record

cd ~

git clone https://github.com/j0lama/ng-scope-docker.git
cd ng-scope-docker
cd docker
sudo docker build -t ngscope .
cd ..
sudo ln -s ng-scope.sh /usr/local/bin/ng-scope